import {sendLogin} from '../api/userAPI'
import {
	LOGIN_BEGIN,
	LOGIN_ERROR,
	LOGIN_SUCCESS,
	LOGOUT
} from "./actionTypes/authActionTypes";

const loginBegin = () => {
	return {
		type: LOGIN_BEGIN,
	};
};

const loginSuccess = (user) => {
	return {
		type: LOGIN_SUCCESS,
		payload: user,
	};
};

const loginError = (error) => {
	return {
		type: LOGIN_ERROR,
		payload: error,
	};
};

export const login = (username, password) => {
	return async (dispatch) => {
		try {
			dispatch(loginBegin())
			let user = await sendLogin(username, password);
			console.log(user);
			localStorage.setItem("user", JSON.stringify(user.data));
			dispatch(loginSuccess(user.data));
		} catch(error) {
			dispatch(loginError("Invalid username or password"))
		}
	};
};

export const logout = () => {
	return{
		type: LOGOUT
	}
}

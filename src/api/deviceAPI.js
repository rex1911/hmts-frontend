import { API_BASE } from "../constants";
import axios from "axios";

export const checkDevice = (deviceID) => {
	const url = API_BASE + "/api/v1/device/verify/" + deviceID;
	return axios.get(url);
};

export const getDeviceData = (token) => {
	const url = API_BASE + "/api/v1/device";
	const headers = {
		Authorization: token,
	};
	return axios.get(url, { headers });
};

import React from "react";
import CardBase from "../CardBase/CardBase";
import { Button, CircularProgress, TextField, Typography } from "@material-ui/core";

import createStyles from "./styles";

const mainForm = ({
	formStep,
	formData,
	onDataChange,
	onSubmit,
	error,
	loading,
}) => {
	const classes = createStyles();

	if (formStep !== 2) {
		return null;
	}

	return (
		<CardBase heading="Enter username and password">
			<div className={classes.mainDiv}>
				<TextField
					label="Username"
					variant="filled"
					value={formData.username}
					onChange={onDataChange}
					name="username"
					fullWidth
				/>
				<TextField
					label="Password"
					variant="filled"
					value={formData.password}
					onChange={onDataChange}
					name="password"
					fullWidth
				/>
				{error ? (
					<Typography variant="caption" display="block" gutterBottom>
						Username is already in use
					</Typography>
				) : null}
				{loading ? (
					<CircularProgress align={"center"}/>
				) : (
					<Button
						variant="contained"
						color="primary"
						fullWidth={true}
						onClick={onSubmit}
						className={classes.nextBtn}
					>
						Next
					</Button>
				)}
			</div>
		</CardBase>
	);
};

export default mainForm;

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
	cardBase: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: 500
	},
	headingText: {
		fontWeight: 500,
		margin: 20,
		textAlign: 'center'
	}
})

export default useStyles
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
	deviceIDdiv: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		paddingBottom: 20,
		"& > *": {
			margin: 10
		}
	}, 
	errorMsg: {
		color: "#C0392B"
	}
});

export default useStyles
import React from "react";
import {
	Button,
	CircularProgress,
	TextField,
	Typography,
} from "@material-ui/core";
import CardBase from "../CardBase/CardBase";

import styles from "./styles";

const deviceID = ({
	formStep,
	formData,
	onDataChange,
	onSubmit,
	loading,
	error,
}) => {
	const classes = styles();

	if (formStep !== 1) {
		return null;
	}

	return (
		<CardBase heading="Enter Device ID">
			<div className={classes.deviceIDdiv}>
				<TextField
					label="Device ID"
					variant="filled"
					value={formData.device}
					onChange={onDataChange}
					name="device"
					fullWidth
					error={error}
				/>

				{error && (
					<Typography variant="caption" display="block" className={classes.errorMsg} gutterBottom>
						This device does not exist
					</Typography>
				)}
				{loading ? (
					<CircularProgress className={classes.deviceIDBtn} />
				) : (
					<Button
						variant="contained"
						color="primary"
						className={classes.deviceIDBtn}
						fullWidth={true}
						onClick={onSubmit}
					>
						Next
					</Button>
				)}
			</div>
		</CardBase>
	);
};

export default deviceID;

import { makeStyles } from "@material-ui/core/styles";

export default makeStyles({
	mainDiv: {
		width: "80%",
		paddingBottom: 20,
		"& > *": {
			margin: 10
		}
	} 
})
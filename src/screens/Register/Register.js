import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import LoginBG from "../../assets/images/LoginBG.jpg";
import Fade from "@material-ui/core/Fade";
import Step1 from "./components/Step1/Step1";
import Step2 from "./components/Step2/Step2";
import Step3 from "./components/Step3/Step3";
import { checkDevice } from "../../api/deviceAPI";
import { createUser, verifyUsername } from "../../api/userAPI";

import { FADE_TIMEOUT, LOGGEDIN } from "../../constants";
import { Redirect } from "react-router";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
	root: {
		backgroundImage: "url(" + LoginBG + ")",
		backgroundSize: "cover",
		backgroundPosition: "center",
		backgroundRepeat: "no-repeat",
		height: "100vh",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},
	backdrop: {
		zIndex: 1,
	},
});

const Register = (props) => {
	const classes = useStyles();

	//State Hooks
	const [formStep, changeFormStep] = useState(1);
	const [formData, changeFormData] = useState({});
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);
	const [success, setSuccess] = useState(false);
	const [fadeIn, setFadeIn] = useState(true)

	const user = useSelector((state) => state.user);
	//Handlers
	const switchStep = stepNo => {
		setFadeIn(false)
		setTimeout(() => {
			changeFormStep(stepNo)
			setFadeIn(true)
		}, FADE_TIMEOUT);
	}
	
	const handleDeviceIDCheck = async () => {
		try {
			setLoading(true);
			setError(false);
			await checkDevice(formData.device);
			switchStep(2)
		} catch (error) {
			setError(true);
			console.log("No such device");
		} finally {
			setLoading(false);
		}
	};

	const handleStep2 = async () => {
		setError(false)
		setLoading(true)
		const res = await verifyUsername(formData.username)
		setLoading(false)
		if(res.data.available === true) {
			switchStep(3)
		} else {
			setError(true)
		}
	};

	const handleStep3 = async () => {
		try {
			setLoading(true);
			await createUser(formData);
			setSuccess(true);
		} catch (error) {
			setError(true);
		} finally {
			setLoading(false);
		}
	};

	const handleFormDataChange = (e) => {
		changeFormData((oldState) => {
			return { ...oldState, [e.target.name]: e.target.value };
		});
	};

	return (
		<React.Fragment>
			{user.loginStatus === LOGGEDIN ? (
				<Redirect to="/dashboard" />
			) : null}
			{success && <Redirect to="/login" />}
			<div className={classes.root}>
				<Fade in={fadeIn? true: false} timeout={FADE_TIMEOUT}>
					<div>
						<Step1
							formStep={formStep}
							formData={formData}
							onDataChange={(e) => handleFormDataChange(e)}
							onSubmit={handleDeviceIDCheck}
							loading={loading}
							error={error}
						/>
						<Step2
							formStep={formStep}
							formData={formData}
							onDataChange={(e) => handleFormDataChange(e)}
							onSubmit={handleStep2}
							loading={loading}
							error={error}
						/>
						<Step3
							formStep={formStep}
							formData={formData}
							onDataChange={(e) => handleFormDataChange(e)}
							onSubmit={handleStep3}
							loading={loading}
						/>
					</div>
				</Fade>
			</div>
		</React.Fragment>
	);
};

export default Register;

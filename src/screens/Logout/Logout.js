import React, {useEffect} from 'react'
import { useDispatch } from 'react-redux'
import { Redirect } from 'react-router-dom'
import {logout} from '../../actions/authActions'



const Logout = () => {
	const dispatch = useDispatch()
	
	useEffect(() => {
		dispatch(logout())
	}, [dispatch])
	
	return (
		<div>
			<Redirect to="/login" />
		</div>
	)
}

export default Logout

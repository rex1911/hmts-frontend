import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import LoginBG from "../../assets/images/LoginBG.jpg";
import Fade from "@material-ui/core/Fade";
import LoginCard from "./components/LoginCard";

import {login} from "../../actions/authActions"

import { FADE_TIMEOUT } from "../../constants";
import { useDispatch, useSelector } from "react-redux";
import {LOGINERROR, LOGINPENDING, LOGGEDIN} from '../../constants'
import { Redirect } from "react-router-dom";

const useStyles = makeStyles({
	root: {
		backgroundImage: "url(" + LoginBG + ")",
		backgroundSize: "cover",
		backgroundPosition: "center",
		backgroundRepeat: "no-repeat",
		height: "100vh",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},
});

const Login = (props) => {
	const classes = useStyles();
	const dispatch = useDispatch()
	//States
	const [username, setUserName] = useState("");
	const [password, setPassword] = useState("");
	
	const user = useSelector(state => state.user)

	const handleLoginClick = async () => {
		dispatch(login(username, password))
	};

	const getData = (data) => {
		if(data === "error") {
			if(user.loginStatus === LOGINERROR) {
				return true
			} else {
				return false
			}
		}

		if(data === "loading") {
			if(user.loginStatus === LOGINPENDING) {
				return true
			} else {
				return false
			}
		}
	} 

	return (
		<React.Fragment>
			{user.loginStatus === LOGGEDIN ? <Redirect to="/dashboard" /> : null}
			<div className={classes.root}>
				<Fade in={true} timeout={FADE_TIMEOUT}>
					<div>
						<LoginCard
							onLoginClick={handleLoginClick}
							username={username}
							onUsernameChange={(e) =>
								setUserName(e.target.value)
							}
							onPasswordChange={(e) =>
								setPassword(e.target.value)
							}
							password={password}
							error={getData('error')}
							loading={getData('loading')}
						/>
					</div>
				</Fade>
			</div>
		</React.Fragment>
	);
};

export default Login;

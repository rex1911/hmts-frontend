import { makeStyles } from "@material-ui/core/styles";

export default makeStyles({
	card: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		padding: "40px 0px",
		height: 250,
		width: 700,
	},
	cardMedia: {
		width: 300,
		height: 300,
		backgroundSize: "contain",
	},
	loginSection: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		marginLeft: 50,
	},
	inputFlex: {
		padding: "10px 0px",
	},
	loginBtn: {
		width: "100%",
		marginTop: 15,
	},
	text: {
		fontWeight: 500,
	},
	errorText: {
		color: "#C0392B"
	}
});

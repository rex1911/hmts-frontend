import React from "react";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import AccountCircle from "@material-ui/icons/AccountCircle";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { CardMedia, CircularProgress, InputAdornment } from "@material-ui/core";
import LoginCardIMG from "../../../assets/images/login1.svg";

import useStyles from "./styles";

const LoginCard = ({
	onLoginClick,
	username,
	onUsernameChange,
	password,
	onPasswordChange,
	error,
	loading,
}) => {
	const classes = useStyles();

	return (
		<Card className={classes.card}>
			<CardMedia image={LoginCardIMG} className={classes.cardMedia} />
			<div className={classes.loginSection}>
				<Typography
					variant="h4"
					display="block"
					gutterBottom
					className={classes.text}
				>
					Login
				</Typography>

				<div className={classes.inputFlex}>
					<span>
						<TextField
							InputProps={{
								startAdornment: (
									<InputAdornment position="start">
										<AccountCircle />
									</InputAdornment>
								),
							}}
							error={error}
							label="Username"
							size="small"
							variant="filled"
							value={username}
							onChange={onUsernameChange}
						/>
					</span>
				</div>

				<div className={classes.inputFlex}>
					<span>
						<TextField
							InputProps={{
								startAdornment: (
									<InputAdornment position="start">
										<VpnKeyIcon />
									</InputAdornment>
								),
							}}
							error={error}
							label="Password"
							type="password"
							size="small"
							variant="filled"
							value={password}
							onChange={onPasswordChange}
						/>
					</span>
				</div>

				{error && (
					<Typography
						variant="caption"
						display="block"
						className={classes.errorText}
						gutterBottom
					>
						Incorrect username or password
					</Typography>
				)}

				{loading ? (
					<CircularProgress className={classes.loginBtn} />
				) : (
					<Button
						variant="contained"
						color="primary"
						className={classes.loginBtn}
						onClick={onLoginClick}
					>
						Login
					</Button>
				)}
			</div>
		</Card>
	);
};

export default LoginCard;

import { Zoom } from '@material-ui/core'
import {ZOOM_TIMEOUT} from '../../constants'
import React from 'react'

function ZoomAdvanced({children, number}) {
	return (
		<Zoom in={true} timeout={ZOOM_TIMEOUT} style={{transitionDelay: `${ZOOM_TIMEOUT * number}ms`}}>
			{children}
		</Zoom>
	)
}

export default ZoomAdvanced

const generateRandom = (min, max) => Math.random() * (max - min) + min;
export default generateRandom;

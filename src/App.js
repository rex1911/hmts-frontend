import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Suspense } from "react";
import Loader from './screens/Loading/Loading'
const Home = React.lazy(() => import("./screens/Home"));
const Login = React.lazy(() => import("./screens/Login"));
const Register = React.lazy(() => import("./screens/Register"));
const Dashboard = React.lazy(() => import("./screens/Dashboard"));
const Logout = React.lazy(() => import("./screens/Logout/Logout"));
const ProtectedRoute = React.lazy(() => import("./hoc/ProtectedRoute"));

function App() {
	return (
		<BrowserRouter>
			<Suspense fallback={<Loader />}>
				<Switch>
					<Route path="/register" exact>
						<Register />
					</Route>
					<Route path="/login" exact>
						<Login />
					</Route>
					<Route path="/logout" exact>
						<Logout />
					</Route>
					<ProtectedRoute path="/dashboard" exact>
						<Dashboard />
					</ProtectedRoute>
					<Route path="/loader" exact>
						<Loader />
					</Route>
					<Route path="/" exact>
						<Home />
					</Route>
				</Switch>
			</Suspense>
		</BrowserRouter>
	);
}

export default App;

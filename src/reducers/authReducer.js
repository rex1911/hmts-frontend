import {
	LOGIN_BEGIN,
	LOGIN_SUCCESS,
	LOGIN_ERROR,
	LOGOUT
} from "../actions/actionTypes/authActionTypes";
import {
	LOGGEDIN,
	LOGGEDOUT,
	LOGINPENDING,
	LOGINERROR,
} from "../constants";

let user = localStorage.getItem("user");
if (user) {
	user = JSON.parse(user);
}
const loginBase = {
	error: "",
};

const initialState = user
	? { loginStatus: LOGGEDIN, ...user, ...loginBase }
	: { loginStatus: LOGGEDOUT, ...loginBase };

const authReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN_BEGIN:
			console.log("Begin")
			return { ...state, loginStatus: LOGINPENDING, error: ""};

		case LOGIN_SUCCESS:
			return { ...state, ...action.payload, loginStatus: LOGGEDIN };

		case LOGIN_ERROR:
			return { ...state, loginStatus: LOGINERROR, error: action.payload};

		case LOGOUT:
			localStorage.removeItem("user")
			return {loginStatus: LOGGEDOUT, ...loginBase}
		default:
			return state;
	}
};

export default authReducer;
